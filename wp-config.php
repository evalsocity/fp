<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'freepathshala');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$>mz%S:0WB|jsgQ::_0wd<8Hyp0PS_Fd)+FsO)je,JtWS21 |ees(w](M($O<QJm');
define('SECURE_AUTH_KEY',  '$w?,<|tCqdf7KcJs+2]F)@*Kv(lN}t0=z7&vuAx{-fW56Y+knB&MB2Vub?`tI#]_');
define('LOGGED_IN_KEY',    'Rm~n{WELbuPH:%>]xCIZ@I3j&RLb~>8`eOHX`$KH7as)e}=1>59,k|(5p*8{na<4');
define('NONCE_KEY',        'K<{DjE%.Nkmr~G}%MS4KlO2VB[GRy!Ab3@q<Y<sN)Do1Z]jYIZD]0e>^NS}e||.I');
define('AUTH_SALT',        'lZk3f)-^xT:&Kr}&_m}m[wwR-&H`8DWQPEe8:7`K|lLysR>M_l{;KOeY>>@QIK*U');
define('SECURE_AUTH_SALT', 'MgGb*%z0&;sG2@n^S =1?pM/3PaDryGJ5{)jup{G-#=Wmx%.MWSj>)b NJB>)B 6');
define('LOGGED_IN_SALT',   'U~Iko^7$%yshG?,k&Z03f&iopM!WlOgJ <3FH<vS1@Bug FP98my-Jyw$a~~-9C]');
define('NONCE_SALT',       'F!S8aUxE^a0Q]FP@9gOSK<59~lcT^}yI5lw^KWRK3XNDxc7$>w+P%C$+kK~9d$$2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wbwp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
